# ubuntu-base
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/ubuntu-base)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/ubuntu-base)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/ubuntu-base/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/ubuntu-base/aarch64)
### armv7
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/ubuntu-base/armv7)
### x64_focal
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/ubuntu-base/x64focal)
### x64_bionic
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/ubuntu-base/x64bionic)



----------------------------------------
#### Description

* Distribution : [Ubuntu](https://www.ubuntu.com/)
* Architecture : x64,aarch64,armv7
* Appplication : -



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/ubuntu-base:[ARCH_TAG]
```



----------------------------------------
#### Usage

```dockerfile
FROM forumi0721/ubuntu-base:[ARCH_TAG]

RUN 'build-code'
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

